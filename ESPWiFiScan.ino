/*
 *  This sketch demonstrates how to scan WiFi networks. 
 *  The API is almost the same as with the WiFi Shield library, 
 *  the most obvious difference being the different file you need to include:
 */
#include "ESP8266WiFi.h"
#include <Wire.h>

extern "C" {
  #include "user_interface.h"
  #include "stdlib_noniso.h"
}

/* 
Setup:

To scan hidden SSIDs, change ESP8266WiFi.h
Location: Arduino.app/hardware/esp8266com/esp8266/libraries/ESP8266WiFi/src

Search for config.show_hidden and change to 1

e.g.
    config.show_hidden = 1;
*/


void setup() {
  Serial.begin(115200);

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  Wire.pins(0, 2);                        //on ESP-01.
  Wire.begin();
  StartUp_OLED();                         // Init Oled and fire up!

  Draw_WIFI();
  delay(200);

  Serial.println("Setup done");

  // http://bbs.espressif.com/viewtopic.php?t=133
  // NONE_SLEEP_T = 50-56 mA base (RX)
  // MODEM_SLEEP_T = 15mA
  // LIGHT_SLEEP_T = 0.5mA
  wifi_set_sleep_type(LIGHT_SLEEP_T);  
}

void loop() {
  Serial.println("scan start");

  clear_display();
  sendStrXY(">>> Scanning <<<", 0, 0);
  
  int n = WiFi.scanNetworks();            // WiFi.scanNetworks will return the number of networks found
  int ln = 0;                             // OLED to print line: 0 - 4
  char *WiFiSSID = "";
  char WiFiRSSI[3];
  int rv;

//  String ssid_scan;
//  int32_t rssi_scan;
//  uint8_t sec_scan;
//  uint8_t* BSSID_scan;
//  int32_t chan_scan;
//  bool hidden_scan;

  // WiFi.scanNetworks will return the number of networks found
  Serial.println("scan done");
  clear_display();
  if (n == 0) {
    Serial.println("no networks found");
    sendStrXY("No Nets in Range", ln++, 0);
  } else  {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i)  {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");

//hidden_scan = true;
//      WiFi.getNetworkInfo(i, ssid_scan, sec_scan, rssi_scan, BSSID_scan, chan_scan, hidden_scan);

      
      // Write the SSID name (if available)
      strcpy (WiFiSSID, WiFi.SSID(i));
      sendStrXY(WiFiSSID, ln, 0);
//      if (strcmp(ssid_scan.c_str(),'\0')) {
//        sendStrXY("<hidden>",ln, 0);
//      } else {
//        strcpy (WiFiSSID, ssid_scan.c_str());
//        sendStrXY(WiFiSSID, ln, 0);
//      }

//      Serial.print(WiFi.SSID(i));
//      Serial.print(" (");
//      Serial.print(WiFi.RSSI(i));
      
      // Write the signal strength
      itoa(WiFi.RSSI(i),WiFiRSSI, 10);
      sendStrXY(WiFiRSSI, ln++, 13);
//      itoa(rssi_scan,WiFiRSSI, 10);
//      sendStrXY(WiFiRSSI, ln++, 13);
      
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");

//sendStr((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":"*");

      delay(10);

      // page through 7 items, on a 7-second delay
      if (i > 0 && i%7 == 0) {
        delay(7000);  
        clear_display();
        ln=0;
      }
      
    }
  }
  Serial.println("");

  // clean up ram
  
  // Wait a bit before scanning again
  delay(7000);
}
